//
//  Player.h
//  AdrianPaddlehands
//
//  Created by Ryan Salton on 27/08/2014.
//  Copyright (c) 2014 Ryan Salton. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>

@interface Player : SKSpriteNode

- (void)setPlayerHead;

@end
