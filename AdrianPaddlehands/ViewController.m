//
//  ViewController.m
//  AdrianPaddlehands
//
//  Created by Ryan Salton on 25/08/2014.
//  Copyright (c) 2014 Ryan Salton. All rights reserved.
//

#import "ViewController.h"
#import "MyScene.h"

@import AVFoundation;

@implementation ViewController {
    SKView *skView;
    AVAudioPlayer *_backgroundMusicPlayer;
}

- (void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];

    // Configure the view.
    if(!skView){
        skView = (SKView *)self.view;
//        skView.showsFPS = YES;
//        skView.showsNodeCount = YES;
        
        // Create and configure the scene.
        SKScene * scene = [MyScene sceneWithSize:skView.bounds.size];
        scene.scaleMode = SKSceneScaleModeAspectFill;
        
        // Present the scene.
        [skView presentScene:scene];
        
        [self setBackgroundMusic:@"Ripples.mp3"];
        [_backgroundMusicPlayer setVolume:1];
        [_backgroundMusicPlayer play];
    }
}

- (void)setBackgroundMusic:(NSString *)filename
{
    NSError *error;
    NSURL *backgroundMusicURL =
    [[NSBundle mainBundle] URLForResource:filename
                            withExtension:nil];
    _backgroundMusicPlayer =
    [[AVAudioPlayer alloc]
     initWithContentsOfURL:backgroundMusicURL error:&error];
    _backgroundMusicPlayer.numberOfLoops = -1;
    [_backgroundMusicPlayer prepareToPlay];
}

- (BOOL)shouldAutorotate
{
    return YES;
}

- (NSUInteger)supportedInterfaceOrientations
{
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
        return UIInterfaceOrientationMaskAllButUpsideDown;
    } else {
        return UIInterfaceOrientationMaskAll;
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Release any cached data, images, etc that aren't in use.
}

@end
