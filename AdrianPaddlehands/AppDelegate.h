//
//  AppDelegate.h
//  AdrianPaddlehands
//
//  Created by Ryan Salton on 25/08/2014.
//  Copyright (c) 2014 Ryan Salton. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
