//
//  Enemy.m
//  AdrianPaddlehands
//
//  Created by Ryan Salton on 28/08/2014.
//  Copyright (c) 2014 Ryan Salton. All rights reserved.
//

#import "Enemy.h"

@implementation Enemy {
    NSInteger timer;
    NSInteger moveAmount;
}

-(instancetype)initWithColor:(UIColor *)color size:(CGSize)size
{
    self = [super initWithColor:color size:size];
    if(self){
        
        timer = 15;
        moveAmount = 20;
        [self moveForward];
    }
    return self;
}

-(void)waitForMoveForward
{
    [self runAction:[SKAction sequence:@[[SKAction waitForDuration:timer], [SKAction customActionWithDuration:0 actionBlock:^(SKNode *node, CGFloat elapsedTime) {
        [self moveForward];
    }]]]];
}

-(void)moveForward
{
    [self runAction:[SKAction moveByX:0 y:-moveAmount duration:0.5] completion:^{
        [self waitForMoveForward];
    }];
}

@end
