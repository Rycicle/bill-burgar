//
//  MyScene.m
//  AdrianPaddlehands
//
//  Created by Ryan Salton on 25/08/2014.
//  Copyright (c) 2014 Ryan Salton. All rights reserved.
//

#import "MyScene.h"
#import "Player.h"
#import "Paddle.h"
#import "Enemy.h"

#define PADDLE_Y_MAX self.size.height * 0.7
#define DEGREES_TO_RADIANS(angle) ((angle) / 180.0 * M_PI)

static const uint32_t ballCategory = 0x1 << 1;
static const uint32_t paddleCategory = 0x1 << 2;
static const uint32_t wallCategory = 0x1 << 3;
static const uint32_t floorCategory = 0x1 << 4;
//static const uint32_t particleCategory = 0x1 << 4;

@implementation MyScene {
    Paddle *leftPaddle;
    Paddle *rightPaddle;
    Player *player;
    SKSpriteNode *floorNode;
    SKSpriteNode *leftWallNode;
    SKSpriteNode *rightWallNode;
    
    SKShapeNode *leftArm;
    SKShapeNode *rightArm;
    
    NSInteger curveControlAdjust;
    NSInteger curveControlAdjustRight;
    
    NSInteger numberOfBurgers;
    
    SKLabelNode *scoreLabel;
    NSInteger score;
    
    SKSpriteNode *gameOverNode;
    BOOL gameOver;
    
    SKAction *punchSound;
    SKAction *screamSound;
}

-(id)initWithSize:(CGSize)size {    
    if (self = [super initWithSize:size]) {
        /* Setup your scene here */
        
        self.backgroundColor = [SKColor colorWithRed:0.9 green:0.55 blue:0.2 alpha:1.0];
        
        self.physicsWorld.gravity = CGVectorMake(0, -1);
        self.physicsWorld.contactDelegate = self;
//        self.physicsBody = [SKPhysicsBody bodyWithEdgeLoopFromRect:self.frame];
//        self.physicsBody.restitution = 1.0;
//        self.physicsBody.friction = 0;
//        self.physicsBody.categoryBitMask = wallCategory;
//        self.physicsBody.collisionBitMask = ballCategory;
        
        floorNode = [SKSpriteNode spriteNodeWithColor:[UIColor greenColor] size:CGSizeMake(self.size.width, 10)];
        floorNode.position = CGPointMake(self.size.width * 0.5, -5);
        floorNode.name = @"Floor";
        floorNode.physicsBody = [SKPhysicsBody bodyWithRectangleOfSize:floorNode.size];
        floorNode.physicsBody.dynamic = NO;
        floorNode.physicsBody.categoryBitMask = floorCategory;
        floorNode.physicsBody.contactTestBitMask = ballCategory;
        floorNode.physicsBody.collisionBitMask = ballCategory;
        [self addChild:floorNode];
        
        leftWallNode = [SKSpriteNode spriteNodeWithColor:[UIColor greenColor] size:CGSizeMake(10, self.size.height * 8)];
        leftWallNode.position = CGPointMake(-20, self.size.height);
        leftWallNode.name = @"Wall";
        leftWallNode.physicsBody = [SKPhysicsBody bodyWithRectangleOfSize:leftWallNode.size];
        leftWallNode.physicsBody.dynamic = NO;
        leftWallNode.physicsBody.categoryBitMask = wallCategory;
        leftWallNode.physicsBody.contactTestBitMask = ballCategory;
        [self addChild:leftWallNode];
        
        rightWallNode = [SKSpriteNode spriteNodeWithColor:[UIColor greenColor] size:CGSizeMake(10, self.size.height * 8)];
        rightWallNode.position = CGPointMake(self.size.width + 20, self.size.height);
        rightWallNode.name = @"Wall";
        rightWallNode.physicsBody = [SKPhysicsBody bodyWithRectangleOfSize:rightWallNode.size];
        rightWallNode.physicsBody.dynamic = NO;
        rightWallNode.physicsBody.categoryBitMask = wallCategory;
        rightWallNode.physicsBody.contactTestBitMask = ballCategory;
        [self addChild:rightWallNode];
        
        player = [Player spriteNodeWithImageNamed:@"player"];
        player.position = CGPointMake(size.width * 0.5, (player.size.height * 0.5));
        
        leftPaddle = [[Paddle alloc] initWithImageNamed:@"paddle-left"];
        leftPaddle.position = CGPointMake(size.width * 0.25, player.position.y + 80);
        leftPaddle.physicsBody.categoryBitMask = paddleCategory;
        leftPaddle.physicsBody.contactTestBitMask = ballCategory;
        leftPaddle.physicsBody.collisionBitMask = ballCategory;
        leftPaddle.physicsBody.restitution = 2.0;
        
        [leftPaddle runAction:[SKAction rotateToAngle:DEGREES_TO_RADIANS(-20) duration:0]];
        
        rightPaddle = [[Paddle alloc] initWithImageNamed:@"paddle-right"];
        rightPaddle.position = CGPointMake(size.width * 0.75, player.position.y + 80);
        rightPaddle.physicsBody.categoryBitMask = paddleCategory;
        rightPaddle.physicsBody.contactTestBitMask = ballCategory;
        rightPaddle.physicsBody.collisionBitMask = ballCategory;
        rightPaddle.physicsBody.restitution = 2.0;
        
        [rightPaddle runAction:[SKAction rotateToAngle:DEGREES_TO_RADIANS(20) duration:0]];
        
        [self drawArms];
        
        [self addChild:player];
        [self addChild:leftPaddle];
        [self addChild:rightPaddle];
        
//        [self createBalls];
//        
        [self createEnemies];
        [player setPlayerHead];
//        [self chooseFace];
        
        score = 0;
        scoreLabel = [SKLabelNode labelNodeWithFontNamed:@"HelveticaNeue-Light"];
        scoreLabel.position = CGPointMake(self.size.width * 0.5, self.size.height - 50);
        [self addChild:scoreLabel];
        
        [self updateScore];
        
        
        gameOverNode = [SKSpriteNode spriteNodeWithColor:[UIColor colorWithRed:213.0/255.0 green:111.0/255.0 blue:0.0/255.0 alpha:1.0] size:CGSizeMake(250, 200)];
        gameOverNode.position = CGPointMake(self.size.width * 0.5, self.size.height *0.5);
        
        SKShapeNode *gameOverBorder = [SKShapeNode node];
        [gameOverBorder setPath:CGPathCreateWithRect(CGRectMake(-gameOverNode.size.width * 0.5, -gameOverNode.size.height * 0.5, gameOverNode.size.width, gameOverNode.size.height), nil)];
        gameOverBorder.fillColor = [UIColor clearColor];
        gameOverBorder.strokeColor = [UIColor colorWithRed:138.0/255.0 green:71.0/255.0 blue:0.0/255.0 alpha:1.0];
        gameOverBorder.lineWidth = 4;
        [gameOverNode addChild:gameOverBorder];
        
        
        [self addChild:gameOverNode];
        
        SKLabelNode *gameOverLabel = [SKLabelNode labelNodeWithFontNamed:@"HelveticaNeue-Light"];
        gameOverLabel.text = @"Game Over";
        gameOverLabel.position = CGPointMake(0, 30);
        [gameOverNode addChild:gameOverLabel];
        
        SKLabelNode *restartLabel = [SKLabelNode labelNodeWithFontNamed:@"HelveticaNeue-Light"];
        restartLabel.text = @"Tap to Restart";
        restartLabel.position = CGPointMake(0, -30);
        [gameOverNode addChild:restartLabel];
        gameOverNode.hidden = YES;
        
        
        
        punchSound = [SKAction playSoundFileNamed:@"human_face_punch.mp3" waitForCompletion:NO];
        screamSound = [SKAction playSoundFileNamed:@"scream.mp3" waitForCompletion:NO];
        
        
    }
    return self;
}

- (void)updateScore
{
    scoreLabel.text = [NSString stringWithFormat:@"%li", (long int)score];
}

-(void)createBalls
{
    CGFloat radius = 8.0;
    
    CGMutablePathRef circlePath = CGPathCreateMutable();
    CGPathAddArc(circlePath, NULL, 0, 0, radius, 0, M_PI * 2, YES);
    
    SKShapeNode *circleNode = [[SKShapeNode alloc] init];
    circleNode.path = circlePath;
    circleNode.position = CGPointMake(self.size.width * 0.5, self.size.height);
    circleNode.fillColor = [UIColor whiteColor];
    [self addChild:circleNode];
    
    circleNode.physicsBody = [SKPhysicsBody bodyWithCircleOfRadius:radius];
    circleNode.physicsBody.restitution = 1;
    circleNode.physicsBody.linearDamping = 0;
    circleNode.physicsBody.friction = 0;
    circleNode.physicsBody.categoryBitMask = ballCategory;
    circleNode.physicsBody.contactTestBitMask = paddleCategory;
    circleNode.physicsBody.collisionBitMask = paddleCategory | wallCategory;
    
    [circleNode.physicsBody applyImpulse:CGVectorMake(2, 2)];
}

-(void)createEnemies
{
    if(gameOver){
        return;
    }
//    for(int i = 0; i < 5; i++){
//        Enemy *enemy = [[Enemy alloc] initWithColor:[SKColor redColor] size:CGSizeMake(50, 50)];
//        enemy.position = CGPointMake(40 + (80 * i), self.size.height + (enemy.size.height * 0.5));
//        [self addChild:enemy];
//    }
    if(!numberOfBurgers){
        numberOfBurgers = 0;
    }
    
    numberOfBurgers ++;
    NSInteger randX = arc4random_uniform(self.size.width * 0.6) + (self.size.width * 0.2);
//    NSLog(@"RANDOM BURGER POS: %li", (long int)randX);
    
    SKSpriteNode *burger = [SKSpriteNode spriteNodeWithImageNamed:@"burger"];
    burger.position = CGPointMake(randX, self.size.height  + 60);
    burger.physicsBody = [SKPhysicsBody bodyWithRectangleOfSize:burger.size];
    burger.physicsBody.categoryBitMask = ballCategory;
    burger.physicsBody.contactTestBitMask = paddleCategory | wallCategory | floorCategory;
    burger.physicsBody.collisionBitMask = paddleCategory | floorCategory | ballCategory;
    burger.physicsBody.restitution = 1.0;
    burger.name = @"Burger";
    [self addChild:burger];
    
    CGFloat burgerDelay = 3.0 - (numberOfBurgers / 10);
    if(burgerDelay < 0.8) {
        burgerDelay = 0.8;
    }
    
    [self runAction:[SKAction sequence:@[[SKAction waitForDuration:burgerDelay], [SKAction runBlock:^{
        [self createEnemies];
    }]]]];
    
}

-(CGFloat)getDegreesRatioBetweenY:(NSInteger)highY andY:(NSInteger)lowY
{
    CGFloat rangeY = PADDLE_Y_MAX - lowY;
    CGFloat degreesPer = rangeY / 60.0;
    CGFloat totalDegrees = (highY * degreesPer) - 90;
    if(totalDegrees < 0){
        totalDegrees = 0;
    }
    return DEGREES_TO_RADIANS(totalDegrees);
}

-(void)movePaddles:(NSSet *)touches
{
    for (UITouch *touch in touches) {
        CGPoint location = [touch locationInNode:self];
        NSInteger yRestrict = PADDLE_Y_MAX;
        
        location.y = location.y + 60;
        
        if(location.y > yRestrict){
            location.y = yRestrict;
        }
        CGFloat rotateAngle = [self getDegreesRatioBetweenY:location.y andY:player.position.y + 60];
        SKSpriteNode *currPaddle;
        
        if(location.x < self.size.width * 0.5){
            currPaddle = leftPaddle;
            rotateAngle = rotateAngle * -1;
//            if(location.x < player.position.x - 120){
//                curveControlAdjust = 0;
//            }
//            else{
//                curveControlAdjust = 120;
//            }
        }
        else{
            currPaddle = rightPaddle;
            
        }
        currPaddle.position = location;
//        [currPaddle runAction:[SKAction rotateToAngle:rotateAngle duration:0]];
        
        
        
        [self drawArms];
    }
}

-(void)drawArms
{
    curveControlAdjust = 60 - (player.position.x - leftPaddle.position.x);
    if(curveControlAdjust < 0){
        curveControlAdjust = 0;
    }
    
    curveControlAdjustRight = 60 - (rightPaddle.position.x - player.position.x);
    if(curveControlAdjustRight < 0){
        curveControlAdjustRight = 0;
    }
    
    
    if(!leftArm){
        leftArm = [SKShapeNode node];
        leftArm.position = CGPointMake(0,0);
        [self addChild:leftArm];
    }
    if(!rightArm){
        rightArm = [SKShapeNode node];
        rightArm.position = CGPointMake(0,0);
        [self addChild:rightArm];
    }
    
    SKColor *armColor = [SKColor colorWithRed:282.0/255.0 green:180.0/255.0 blue:149.0/255.0 alpha:1.0];
    
    CGMutablePathRef leftArmPath = CGPathCreateMutable();
    CGPathMoveToPoint(leftArmPath, nil, player.position.x, player.position.y + 18);
    CGPathAddQuadCurveToPoint(leftArmPath, nil, leftPaddle.position.x - curveControlAdjust, player.position.y, leftPaddle.position.x, leftPaddle.position.y - 5);
    leftArm.path = leftArmPath;
    leftArm.strokeColor = armColor;
    leftArm.lineWidth = 16;
    
    CGMutablePathRef rightArmPath = CGPathCreateMutable();
    CGPathMoveToPoint(rightArmPath, nil, player.position.x, player.position.y + 18);
    CGPathAddQuadCurveToPoint(rightArmPath, nil, rightPaddle.position.x + curveControlAdjustRight, player.position.y, rightPaddle.position.x, rightPaddle.position.y - 5);
    
    rightArm.path = rightArmPath;
    rightArm.strokeColor = armColor;
    rightArm.lineWidth = 16;
}

- (void)gameOver
{
    [self enumerateChildNodesWithName:@"Burger" usingBlock:^(SKNode *node, BOOL *stop) {
        [node removeFromParent];
    }];
    
    gameOver = YES;
    gameOverNode.hidden = NO;
}

-(void)restartGame
{
    score = 0;
    numberOfBurgers = 0;
    [self updateScore];
    gameOverNode.hidden = YES;
    gameOver = NO;
    [self createEnemies];
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    /* Called when a touch begins */
    
    if(gameOver) {
        [self restartGame];
    }
    
    [self movePaddles:touches];
}

-(void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self movePaddles:touches];
}

-(void)update:(CFTimeInterval)currentTime {
    /* Called before each frame is rendered */
    
    
    
}

-(void)didBeginContact:(SKPhysicsContact *)contact
{
    NSLog(@"%@ hit %@", contact.bodyA.node.name, contact.bodyB.node.name);
    if ([contact.bodyA.node.name isEqualToString:@"Floor"] || [contact.bodyB.node.name isEqualToString:@"Floor"])
    {
        [self gameOver];
    }
    else if ([contact.bodyA.node.name isEqualToString:@"Wall"] || [contact.bodyB.node.name isEqualToString:@"Wall"])
    {
        score++;
        [self updateScore];
        [self runAction:screamSound];
    }
    else
    {
//        NSLog(@"BURGER HIT");
        [self runAction:punchSound];
//        SKSpriteNode *burger = [contact.bodyA isEqual:(id)]
    }
    
    
}

@end
