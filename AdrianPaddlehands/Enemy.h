//
//  Enemy.h
//  AdrianPaddlehands
//
//  Created by Ryan Salton on 28/08/2014.
//  Copyright (c) 2014 Ryan Salton. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>

@interface Enemy : SKSpriteNode

-(void)moveForward;

@end
