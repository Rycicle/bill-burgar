//
//  Player.m
//  AdrianPaddlehands
//
//  Created by Ryan Salton on 27/08/2014.
//  Copyright (c) 2014 Ryan Salton. All rights reserved.
//

#import "Player.h"

@implementation Player {
    SKSpriteNode *leftArm;
    SKSpriteNode *rightArm;
    
    SKSpriteNode *playerHead;
}

- (void)setPlayerHead
{
    NSInteger randHead = arc4random_uniform(4);
    NSString *randImageName = [NSString stringWithFormat:@"head%li", (long int)randHead];
    
    if(!playerHead) {
        playerHead = [SKSpriteNode spriteNodeWithImageNamed:randImageName];
        playerHead.position = CGPointMake(0, 0 + (self.size.height * 0.5) + 25);
        [self addChild:playerHead];
    }
    else {
        playerHead.texture = [SKTexture textureWithImageNamed:randImageName];
    }
    
    
    [self runAction:[SKAction sequence:@[[SKAction waitForDuration:1.0], [SKAction runBlock:^{
        [self setPlayerHead];
    }]]]];
}

@end
